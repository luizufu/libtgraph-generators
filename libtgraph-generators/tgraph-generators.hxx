#pragma once

#include <cppcoro/generator.hpp>
#include <libtgraph-generators/export.hxx>

namespace tgraph_generators
{
using edge = std::pair<uint32_t, uint32_t>;
using interval = std::pair<uint32_t, uint32_t>;
using tedge_stream = cppcoro::generator<std::pair<edge, interval>>;

// clang-format off

LIBTGRAPH_GENERATORS_SYMEXPORT
auto make_price(uint32_t n, uint32_t c, uint32_t t, uint32_t a = 1) -> tedge_stream;

LIBTGRAPH_GENERATORS_SYMEXPORT
auto make_emeg(uint32_t n, uint32_t t, float bp, float dp) -> tedge_stream;

// clang-format on

} // namespace tgraph_generators
