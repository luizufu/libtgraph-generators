#include <cassert>
#include <libtgraph-generators/tgraph-generators.hxx>
#include <limits>
#include <random>
#include <unordered_set>
#include <vector>

namespace tgraph_generators
{
struct edge_hash
    {
    public:
    template<typename T, typename U>
    auto operator()(const std::pair<T, U>& x) const -> std::size_t
    {
        return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
    }
    };

static auto generator() -> std::mt19937&;
static auto next_probability() -> float;

auto make_price(uint32_t n, uint32_t c, uint32_t t, uint32_t a) -> tedge_stream // NOLINT
{
    std::vector<uint32_t> in_degrees(n * c);
    float threshold = c / static_cast<float>(c + a);

    uint32_t size = 0;
    for(; size < c; ++size)
    {
        in_degrees[size] = size;
    }

    for(uint32_t u = c; u < n; ++u)
    {
        // to check if new random edges were not yielded before
        std::unordered_set<edge, edge_hash> edges;

        // every node has out-degree exactly c, no fluctuation
        while(edges.size() < c)
        {
            uint32_t v = next_probability() < threshold
                ? in_degrees[next_probability() * size]
                : next_probability() * static_cast<float>(u - 1);

            edge e = {u, v};
            if(edges.find(e) == edges.end())
            {
                edges.insert(e);
                in_degrees[size++] = v;
            }
        }
    }

    std::vector<uint32_t> timestamps(n * c, 0);
    std::vector<bool> active(n * c, true);

    for(uint32_t t1 = 0; t1 < t; ++t1)
    {
        uint32_t i = next_probability() * size;

        if(active[i])
        {
            uint32_t u = i / static_cast<float>(c);
            uint32_t v = in_degrees[i];
            uint32_t t0 = timestamps[i];
            co_yield {edge {u, v}, interval {t0, t1}};

            active[i] = false;
        }
        else
        {
            uint32_t new_v =
            next_probability() < threshold
                ? in_degrees[next_probability() * size]
                : next_probability() * static_cast<float>(n - 1);

            in_degrees[i] = new_v;
            timestamps[i] = t1;
            active[i] = true;
        }
    }

    for(size_t i = 0; i < size; ++i)
    {
        if(active[i])
        {
            uint32_t u = i / static_cast<float>(c);
            uint32_t v = in_degrees[i];
            uint32_t t0 = timestamps[i];
            co_yield {edge {u, v}, interval {t0, t}};
        }
    }
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp) -> tedge_stream // NOLINT
{
    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(int t1 = 1; t1 < t; ++t1)
    {
        for(int u = 0; u < n; ++u)
        {
            for(int v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        co_yield {edge {u, v}, interval {g[u * n + v], t1}};
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                co_yield {edge {u, v}, interval {g[u * n + v], t}};
            }
        }
    }
}

static auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
    static std::mt19937 gen(seed);

    return gen;
}

static auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

} // namespace tgraph_generators
