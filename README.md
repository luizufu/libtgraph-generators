# libtgraph-generators

C++ library

## dependencies

cppcoro


## after commands

    bdep init -C {build-out}/libtgraph-generators-clang @clang cc config.cxx=clang++ \
        config.cxx.coptions = -stdlib=libc++                             \
        config.cc.poptions=-I{build-unpkg}/include                       \
        config.cc.loptions=-L{build-unpkg}/lib

where {build-out} can be relative and {build-unpkg} not
